# 26_DOCKER_KUBERNETES
> Pertemuan : 26. Pengenalan Docker _ Kubernetes | Pak Ruki

<details>
<summary>"Lihat Tugas"</summary>

```
[x] 1. Silakan ikuti tutorial di https://docker-curriculum.com/ sampai bagian Static Sites selesai (sampai Anda askses halaman yang ditampilkan pada tutorial tersebut di local computer Anda)
[ ] 2. Buat docker image yang berisi aplikasi Go yang Anda buat 2 hari lalu (backend API bank). Kumpulkan screenshot yang menunjukkan bahwa image sudah berhasil dibuat.
[ ] 3. Buat docker image yang berisi server database mysql, dan pindahkan database bank yang Anda buat 2 hari lalu ke server tersebut. Kumpulkan screnshot yang menunjukkan bahwa image sudah berasil dibuat.
```

</details>

---

## >> `DOCKER SESSION`
<details>
<summary>"Lihat Docker"</summary>

#### `installasi`
- Install dari [docker.hub](https://hub.docker.com/editions/community/docker-ce-desktop-windows/)
- jangan lupa liat requirement, beda (stable | edge) dan (Win 10 Pro | Home)
-	Kemudian klick 2x installer.exe nya
-	run and close proses installasi
-	Restart komputernya

<details>
<summary>**ERROR** di installasi. Klik sini untuk lihat foto</summary>

![Installasi_Docker](/uploads/c4a5f80cb65cbe6bb0591efcf73282a0/Installasi_Docker.JPG)
>> **[solved]** meminta install [WSL2 Kernel Linux](https://docs.microsoft.com/en-gb/windows/wsl/wsl2-kernel)

</details>

- run and finish
- kemudian restart docker

![Installasi_restart_setelah_wsl2_kernel_linux](/uploads/ce83bf51271e29ef43a92be411256be0/Installasi_restart_setelah_wsl2_kernel_linux.JPG)
-	Berhasil dijalankan! 

---

#### `mencoba command dan fitur docker`
-	langsung otomatis setting path. Jadi coba run di cmd biasa dengan 
```
$ docker --version
$ docker images
```

---

##### --- mencoba `hello-world` container
- coba command ```docker run hello-world```, dia akan pull (hello-world image) otomasis secara online

<details>
<summary>**ERROR** foto</summary>

>> di cmd awalnya memang `unable`, tapi **tunggu** saja.
>>> docker mengecek local terlebih dahulu, baru pull online.

![docker_hello-world](/uploads/6fee76db4141bf9fd5a03df2d29b6844/docker_hello-world.JPG)

</details>

---

##### --- mencoba `busybox` container
- coba `pull` template images dari docker. [ikutin tutorial for dummies](https://docker-curriculum.com/) 
- coba `run` busybox seperti kode dibawah ini
```
$ docker pull busybox
$ docker images
$ docker run busybox
$ docker run busybox echo "hello from busybox"
```
- untuk melihat images yang sedang running
```
$ docker ps
$ docker ps -a
$ docker run -it busybox sh
```
- untuk hapus images sekaligus |  hapus sekaligus dengan kondisi | hapus semua containers yang stop
```
$ docker rm angkacontainerID-1 angkacontainerID-2
$ docker rm $(docker ps -a -q -f status=exited)
$ docker container prune
```

</details>

---

#### `[TUGAS no. 1]`
> docker running di Nginx. mencoba [static-site](https://hub.docker.com/r/prakhar1989/static-site/) container

<details>
<summary>"Lihat no.1"</summary>

- kode dibawah menunjukkan `pull` dan `run link/site` memiliki tungsi yang sampai
- lain ceritanya jika menggunakan `-rm`
>>`-rm` untuk "sementara". otomatis removed jika restart docker. 
```
$ docker pull prakhar1989/static-site
$ docker run prakhar1989/static-site
$ docker run --rm prakhar1989/static-site
```
![pull_static_site](/uploads/c66978183e7da23b9bdc61516cc24f1a/pull_static_site.JPG)
>> Ingat! tiap ada tulisan `unable`, **jangan panik dulu** langsung Ctrl+C wkwkwk kaya aku.
tunggu aja, dia lama fetch nyaa. kalau sudah :
>>> **Nginx is running...**,
>>> maka container tidak muncul di desktop. kill dulu dengan Ctrl + C

- matikan dlu containernya dengan `prune`
- kemudian jalankan container ```docker run --rm prakhar1989/static-site```
- kill terminal
- jalankan ```docker run -d -P --name static-site prakhar1989/static-site```
>> -d untuk detach
>> -p untuk publish
>> -name untuk kasih nama, optional
- lihat portnya ```docker port static-site```
>> 80 -> berhasil

>> 443 -> gagal
![berhasil_docker_statistic-page_running_on_Nginx](/uploads/0d1f05360258fe24f1053bee0820e487/berhasil_docker_statistic-page_running_on_Nginx.JPG)
```
80/tcp -> localhost:32769
443/tcp -> 0.0.0.0:32768
```

- berhenti melalui cmd 
```
docker stop static-site
```

---

<details>
<summary>**Tambahan Lainnya...**</summary>
menggunakan **cli** di docker-desktop (di running container)
``` 
# ls
# dir
# pwd
# cd bin
```
![CLI_docker-desktop](/uploads/9ef919becf111f7a5967d755a3e043b5/CLI_docker-desktop.JPG)
</details>

- selesai.

You're doing an awesome work!

</details>

---

#### `[TUGAS no.2]`
> menampilkan 3 `image` container yang  dalam simple_bank

1. image untuk app yang berbasis [mysql](https://gitlab.com/rulisastra/24_mvc_golang)
2. image untuk app yang berbasis [golang](https://gitlab.com/rulisastra/24_mvc)
3. image untuk app yang berbasis [node.js --> react](https://gitlab.com/rulisastra/25_mvc_frontend)

<details>
<summary>"Lihat no.2"</summary>

- buka direktori golang di VSCode
- buat file baru dengan nama `Dockerfile`
- ikutin tutorial [golang_docker for dummies](https://hub.docker.com/_/golang/)
</details>

---

#### `[TUGAS no.3]`
> membuat database mysql, import [server mysql](https://gitlab.com/rulisastra/24_mvc_golang) ke database baru

<details>
<summary>"Lihat no.3"</summary>

- of course you can do it! even though your eyes are red already

</details>

---

## >> `KUBERNETES SESSION`

<details>
<summary>"Lihat Kubernetes"</summary>

[ ] comming soon ~

</details>

---

## >> `AWS SESSION`
<details>
<summary>"Lihat AWS"</summary>

> `error` jika tidak memungkinkan menggunakan Windows 10 Pro, gunakan AWS. tapi memang jalannya lebih panjang

[ ] comming soon ~

</details>

Awesome work!

</details>

---

### >> `further learning ~`
- Belajar golang [Cloud Native DevOps](https://github.com/cloudnativedevops/demo/tree/master/hello)
- markdown
```
<details>
<summary>"Klik saya"</summary>
You're doing an awesome work!
</details>
```